export default {
    state: {
      choosenProducts: [],
    },
    actions: {
      addProduct(ctx, data) {
        ctx.commit('updateProductList', data)
      },
    },
    mutations: {
      updateProductList(state, data) {
        state.choosenProducts.push(data)
      },
    },
    getters: {
      getProducts(state) {
        return state.choosenProducts
      },
      getTotalPrice(state) {
        if (state.choosenProducts) {
            return state.choosenProducts.reduce((acc, cur) => acc += cur.data.price * cur.data.count)
        }
        
      }
    },
  }