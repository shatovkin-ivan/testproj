import { createStore } from 'vuex'
import add from './modules/add'

const store = createStore({
  modules: {
    add,
  },
})

export default store